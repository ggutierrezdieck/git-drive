import yaml
from yaml.loader import SafeLoader


# https://dev.to/developertharun/yaml-tutorial-using-yaml-with-python-pyyaml-443d

def load_config():
    with open("./conf/config.yml", "r") as ymlfile:
        return yaml.load(ymlfile, SafeLoader)

def get_projects():
    cfg = load_config()
    for project in cfg["projects"]:
        yield project['path'], project['commands']

if __name__ == "__main__":
    cfg = load_config()
    # for section in cfg:
    #     print(section)
    for project in cfg["projects"]:
        print(project['path'])
        # print(project['commands'])

    for p in get_projects():
        print(p)
