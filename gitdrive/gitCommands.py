import os

# from posixpath import basename
# from git import Repo
# import path_scanner
# # based on blog https://medium.com/@tzuni_eh/git-commit-with-gitpython-and-ssh-key-926cad91ca89


class Git(object):

    def __init__(self,path = os.getcwd()):
        self.path = path

    def status(self):
        # TODO: capture FileNotFoundError: [Errno 2] No such file or directory:
        os.chdir(os.path.expanduser(self.path))
        output = os.popen('git status')
        print(output.read())

    def init(self):
        os.chdir(os.path.expanduser(self.path))
        output = os.popen('git init')
        print(output.read)

    def add(self):
        os.chdir(os.path.expanduser(self.path))
        output = os.popen('git add .')
        print(output.read())

    def commit(self, message):
        os.chdir(os.path.expanduser(self.path))
        output = os.popen('git commit -m \"' + message + '\"')
        print(output.read())

    def log(self):
        os.chdir(os.path.expanduser(self.path))
        output = os.popen('git log')
        print(output.read())

    def push(self):
        os.chdir(os.path.expanduser(self.path))
        output = os.popen('git push')
        print(output.read())
    
    def pull(self):
        os.chdir(os.path.expanduser(self.path))
        output = os.popen('git pull')
        print(output.read())

if __name__ == '__main__':
    test_path = '/tmp/pythonTest'
    if not os.path.exists(test_path):
        os.mkdir(test_path)
    else:
        os.chdir(test_path)    
    git = Git(test_path) 
    git.status()
    git.init()

    from datetime import datetime
    now = datetime.now()
    message = 'Automatic backup created on ' + now.strftime("%d/%m/%Y %H:%M:%S")
    test_file = "".join(now.strftime("%H:%M:%S").split(':')[:])
    if not os.path.exists(test_path + test_file):
        print(test_file)
        f = open(test_file, mode='x')
        f.write(message)
        f.close()
  
    git.status()
    print('add')
    git.add()
    print('commit')
    git.commit(message)
    print('log')
    git.log()
