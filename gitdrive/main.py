import yaml
from yaml.loader import SafeLoader
from gitCommands import Git
from datetime import datetime
import os


def load_config(config_file):
    # load config from multiple loations
    # ./conf, ~, /etc/gitdrive, or env var MYPROJECT_CONF
    for loc in os.path.join(os.path.dirname(os.path.realpath(__file__)),'conf'), os.path.expanduser("~"), "/etc/gitdrive", os.environ.get("MYPROJECT_CONF"):
        try: 
            with open(os.path.join(loc,config_file)) as source:
                return yaml.load(source, SafeLoader)
        except (IOError, TypeError):
            pass

def get_projects():
    cfg = load_config()
    for project in cfg["projects"]:
        yield project['path'], project['commands']


if __name__ == '__main__':
    cfg = load_config('config.yml')
    now = datetime.now()
    message = 'Automatic backup created on ' + now.strftime("%d/%m/%Y %H:%M:%S")
    
    for project in cfg["projects"]:
        path = project['path']
       
    # for path in path_scanner.scan(test_paths):
        print(path)
        git = Git(path) 
        # git.status()
        # git.init()
        git.status()
        git.add()
        git.commit(message)
        # git.log()
        git.push()
