import os

def scan(paths):
    for path in paths:
        if not os.path.exists(path):
            print('Path ' + path + ' does not exists')
        else:
            yield path



if __name__ == '__main__':
    test_paths = [
        '/tmp/pythonTest',
        '/Users/ggutierrezdieck/personal/local projects/GitDrive',
        '/Users/ggutierrezdieck/personal/Cursos/_Notes1',
    ]
    
    for path in scan(test_paths):
        print(path)
